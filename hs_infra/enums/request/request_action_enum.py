from hs_infra.enums.base.base_enum import BaseEnum


class RequestActionEnum(BaseEnum):
    get = 'get'
    post = 'post'
    put = 'put'
    delete = 'delete'
